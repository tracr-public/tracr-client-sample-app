﻿using TracrClient;
using TracrClient.Extensions;
using Microsoft.Extensions.DependencyInjection;
using TracrClient.Models;
using Serilog;
using Serilog.Events;
using Microsoft.Extensions.Configuration;

namespace TestApp
{
    internal static class Program
    {
        private static IServiceProvider _serviceProvider;
        public static IConfigurationRoot configuration;

        public static async Task<List<Diamond>> CreatePolishedDiamondFromSplitAsync(TracrService tracrClient)
        {
            var results = new List<Diamond>();

            for (int i = 0; i < 3; i++)
            {
                // ROUGH DIAMOND

                // Create a Rough Diamond
                var newRoughDiamond = new Diamond()
                {
                    ParticipantTimestamp = DateTime.UtcNow,
                    ParticipantId = Guid.NewGuid().ToString(),
                    BoxId = "9999",
                    SightNo = 1,
                    SightYear = 2022,
                    Rough = new Rough
                    {
                        Carats = 8.00
                    },
                };

                var roughDiamond = await tracrClient.CreateRoughDiamondAsync(newRoughDiamond);
                Log.Information($"New Rough diamond {roughDiamond.DiamondId} created");

                // Append an image to the rough diamond
                Log.Information($"Append image ...");
                await tracrClient.AppendImageFileAsync(
                    roughDiamond.DiamondId,
                    "./data/rough/demo_rough_1.jpg",
                    "Rough Diamond Picture",
                    ImageTypeEnum.Default
                );

                // Append a video to the rough diamond
                Log.Information($"Append video ...");
                await tracrClient.AppendVideoFileAsync(
                    roughDiamond.DiamondId,
                    "./data/rough/demo_rough_1.mp4",
                    "Rough Diamond Video"
                );
            
                // SPLIT DIAMOND

                // Split parent diamond
                await tracrClient.SplitDiamondAsync(roughDiamond.DiamondId, DateTime.UtcNow);

                // Create one child
                var childRoughDiamond = new Diamond()
                {
                    ParticipantTimestamp = DateTime.UtcNow,
                    ParticipantId = Guid.NewGuid().ToString(),
                    Rough = new Rough
                    {
                        Carats = 5.123
                    },
                };

                childRoughDiamond = await tracrClient.CreateRoughChildDiamondAsync(roughDiamond.DiamondId, childRoughDiamond);
                Log.Information($"New child Rough diamond {childRoughDiamond.DiamondId} created");

                // Append an image to the rough diamond
                Log.Information($"Append image ...");
                await tracrClient.AppendImageFileAsync(
                    childRoughDiamond.DiamondId,
                    "./data/split/split_demo_1.jpg",
                    "Split Diamond Picture",
                    ImageTypeEnum.Default
                );

                // POLISHED DIAMOND
                Random r = new Random();

                var polishedDiamond = new Diamond()
                {
                    ParticipantTimestamp = DateTime.UtcNow,
                    Polished = new Polished
                    {
                        Carats = Math.Round(1.0 + r.NextDouble() * 3.5, 3),
                        Colour = ColourEnum.D,
                        FluorescenceColour = FluorescenceColourEnum.N,
                        CutGrade = CutGradeEnum.VG,
                        Shape = ShapeEnum.Round,
                        Clarity = ClarityEnum.VVS2,
                        Symmetry = SymmetryEnum.VG,
                        PolishQuality = PolishQualityEnum.EX,
                        Length = Math.Round(r.NextDouble() * (6.86 - 6.80) + 6.80, 2),
                        Width = Math.Round(r.NextDouble() * (6.86 - 6.80) + 6.80, 2),
                        Depth = Math.Round(r.NextDouble() * (4.30 - 4.20) + 4.20, 2),
                        DepthPercent = Math.Round(r.NextDouble() * (63 - 62) + 62, 1),
                        TablePercent = 57,
                        PavillionAngle = 40.8,
                        GirdleThicknessFrom = GirdleThicknessEnum.SlightlyThin
                    }
                };

                polishedDiamond = await tracrClient.PolishDiamondAsync(childRoughDiamond.DiamondId, polishedDiamond);

                Log.Information($"Diamond Polish operation success");

                Log.Information($"Append Grading Report");
                await tracrClient.AppendGradingCertificateAsync(
                    polishedDiamond.DiamondId,
                    "./data/polished/demo_polished_1.pdf",
                    GradingLabEnum.GIA,
                    new DateTime(),
                    "GIA 2215291120",
                    "2215291120",
                    "GIA 2215291120"
                );

                Log.Information($"Append images");
                await tracrClient.AppendImageFileAsync(
                    polishedDiamond.DiamondId,
                    "./data/polished/demo_polished_1.jpg",
                    "Polished Diamond Picture",
                    ImageTypeEnum.Default
                );

                await tracrClient.AppendImageFileAsync(
                    polishedDiamond.DiamondId,
                    "./data/polished/demo_polished_1_Arrows.jpg",
                    "Polished Diamond Picture",
                    ImageTypeEnum.Arrow
                );

                await tracrClient.AppendImageFileAsync(
                    polishedDiamond.DiamondId,
                    "./data/polished/demo_polished_1_heart.jpg",
                    "Polished Diamond Picture",
                    ImageTypeEnum.Heart
                );

                Log.Information($"Append video");
                await tracrClient.AppendVideoFileAsync(
                    polishedDiamond.DiamondId,
                    "./data/polished/demo_polished_1.mp4",
                    "Polished Diamond videos"
                );

                Log.Information($"Append Scan details");
                await tracrClient.AppendScanFileAsync(
                    polishedDiamond.DiamondId,
                    "./data/polished/demo_polished_1.stl"
                );

                await tracrClient.AppendScanReportFileAsync(
                    polishedDiamond.DiamondId,
                    "./data/polished/demo_polished_1.xml"
                );

                results.Add(polishedDiamond);
            }

            return results;
        }

        public static async Task CreatePolishedDiamondFromRough(TracrService tracrClient)
        {
            // ROUGH DIAMOND

            // Create a Rough Diamond
            var newRoughDiamond = new Diamond()
            {
                ParticipantTimestamp = DateTime.UtcNow,
                ParticipantId = Guid.NewGuid().ToString(),
                BoxId = "9999",
                SightNo = 1,
                SightYear = 2022,
                Rough = new Rough
                {
                    Carats = 5.00
                },
            };

            var roughDiamond = await tracrClient.CreateRoughDiamondAsync(newRoughDiamond);
            Log.Information($"New Rough diamond {roughDiamond.DiamondId} created");

            // Append an image to the rough diamond
            Log.Information($"Append image ...");
            await tracrClient.AppendImageFileAsync(
                roughDiamond.DiamondId,
                "./data/rough/demo_rough_1.jpg",
                "Rough Diamond Picture",
                ImageTypeEnum.Default
            );

            // Append a video to the rough diamond
            Log.Information($"Append video ...");
            await tracrClient.AppendVideoFileAsync(
                roughDiamond.DiamondId,
                "./data/rough/demo_rough_1.mp4",
                "Rough Diamond Video"
            );

            // POLISHED DIAMOND
            Random r = new Random();

            var polishedDiamond = new Diamond()
            {
                ParticipantTimestamp = DateTime.UtcNow,
                Polished = new Polished
                {
                    Carats = Math.Round(1.0 + r.NextDouble() * 3.5, 3),
                    Colour = ColourEnum.D,
                    FluorescenceColour = FluorescenceColourEnum.N,
                    CutGrade = CutGradeEnum.VG,
                    Shape = ShapeEnum.Round,
                    Clarity = ClarityEnum.VVS2,
                    Symmetry = SymmetryEnum.VG,
                    PolishQuality = PolishQualityEnum.EX,
                    Length = Math.Round(r.NextDouble() * (6.86 - 6.80) + 6.80, 2),
                    Width = Math.Round(r.NextDouble() * (6.86 - 6.80) + 6.80, 2),
                    Depth = Math.Round(r.NextDouble() * (4.30 - 4.20) + 4.20, 2),
                    DepthPercent = Math.Round(r.NextDouble() * (63 - 62) + 62, 1),
                    TablePercent = 57,
                    PavillionAngle = 40.8,
                    GirdleThicknessFrom = GirdleThicknessEnum.SlightlyThin
                }
            };

            polishedDiamond = await tracrClient.PolishDiamondAsync(roughDiamond.DiamondId, polishedDiamond);

            Log.Information($"Diamond Polish operation success");

            Log.Information($"Append Grading Report");
            var reportId = r.Next(215290000, 215300000);
            await tracrClient.AppendGradingCertificateAsync(
                polishedDiamond.DiamondId,
                "./data/polished/demo_polished_1.pdf",
                GradingLabEnum.GIA,
                new DateTime(),
                $"GIA {reportId.ToString()}",
                reportId.ToString(),
                $"GIA {reportId.ToString()}"
            );

            Log.Information($"Append images");
            await tracrClient.AppendImageFileAsync(
                polishedDiamond.DiamondId,
                "./data/polished/demo_polished_1.jpg",
                "Polished Diamond Picture",
                ImageTypeEnum.Default
            );

            await tracrClient.AppendImageFileAsync(
                polishedDiamond.DiamondId,
                "./data/polished/demo_polished_1_Arrows.jpg",
                "Polished Diamond Picture",
                ImageTypeEnum.Arrow
            );

            await tracrClient.AppendImageFileAsync(
                polishedDiamond.DiamondId,
                "./data/polished/demo_polished_1_heart.jpg",
                "Polished Diamond Picture",
                ImageTypeEnum.Heart
            );

            Log.Information($"Append video");
            await tracrClient.AppendVideoFileAsync(
                polishedDiamond.DiamondId,
                "./data/polished/demo_polished_1.mp4",
                "Polished Diamond videos"
            );

            Log.Information($"Append Scan details");
            await tracrClient.AppendScanFileAsync(
                polishedDiamond.DiamondId,
                "./data/polished/demo_polished_1.stl"
            );

            await tracrClient.AppendScanReportFileAsync(
                polishedDiamond.DiamondId,
                "./data/polished/demo_polished_1.xml"
            );
        }

        public static async Task Main(string[] args)
        {
            // Using .Net Core Dependency Injection
            RegisterServices();

            // Setup Logger
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .CreateLogger();

            var tracrClient = _serviceProvider.GetService<TracrService>();

            // Generate few polished diamonds
            var polishedDiamonds = await CreatePolishedDiamondFromSplitAsync(tracrClient);

            // Create Shipment
            var diamondIds = polishedDiamonds.Select(l => l.DiamondId).ToList();
            string receiver = "https://api.67a74974-9568-437e-a0d6-29832526c307.uat.tracr.com"; // Retailer UAT
            string shipmentRef = $"Shipment_{DateTime.Now.ToString("MM-dd-yyyy_HH-mm")}";

            TransferOutStartResponse transferOutStartDiamond = await tracrClient.TransferOutStartAsync(shipmentRef, receiver, diamondIds);


            DisposeServices();
        }

        private static void RegisterServices()
        {
            var services = new ServiceCollection();
            string configFile = Environment.GetEnvironmentVariable("TRACR_CLI_CONFIG_FILE") ?? $"appsettings.json";

            if (!File.Exists(configFile))
            {
                Log.Error($"Unable to open {configFile}.");
            }

            // Build configuration
            configuration = new ConfigurationBuilder()
                .AddJsonFile(configFile, false)
                .Build();

            // Add access to generic IConfigurationRoot
            services.AddSingleton<IConfigurationRoot>(configuration);

            services.AddLogging();

            services.AddTracrClient(new TracrClientOptions()
            {
                ApiUrl= configuration["TracrApi:Url"],
                TokenUrl = configuration["TracrApi:OAuthToken:TokenUrl"],
                ClientId = configuration["TracrApi:OAuthToken:ClientId"],
                ClientSecret = configuration["TracrApi:OAuthToken:ClientSecret"],
            }
            );

            _serviceProvider = services.BuildServiceProvider();
        }

        private static void DisposeServices()
        {
            switch (_serviceProvider)
            {
                case null:
                    return;
                case IDisposable disposable:
                    disposable.Dispose();
                    break;
            }
        }
    }
}
