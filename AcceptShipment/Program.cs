﻿using TracrClient;
using TracrClient.Extensions;
using Microsoft.Extensions.DependencyInjection;
using TracrClient.Models;
using Serilog;
using Serilog.Events;
using Microsoft.Extensions.Configuration;

namespace TestApp
{
    internal static class Program
    {
        private static IServiceProvider _serviceProvider;
        public static IConfigurationRoot configuration;

        public static async Task TransferInAccept(TracrService tracrClient)
        {
            List<TransferInAcceptItem> diamondIdsAccept = new List<TransferInAcceptItem>();
            diamondIdsAccept.Add(new TransferInAcceptItem
            {
                DiamondId = "clg3ka6v8000109u8vkdgkbmx",
                InscriptionNumber = "GSI89247000101"
            }
            );

            List<TransferInAcceptResponseItem> transferInAcceptDiamond = await tracrClient.TransferInAcceptAsync("cljdw16nn001x4lj1aevuepqp", diamondIdsAccept);

            Log.Information($"Diamond Transfer In Accept operation success");
        }

        public static async Task GetMultipleInboundShipment(TracrService tracrClient)
        {
            try
            {
                //Get the shipment details for multiple shipment id
                List<InboundShipment> shipments = await tracrClient.GetAllInboundShipmentsAsync();
                Log.Information($"Get Multiple Inbound Shipment operation success");
            }
            catch (Exception ex)
            {
                Log.Information($"Get Multiple Inbound Shipment operation success but no list of Shipment ID exist");
            }
        }

        public static async Task Main(string[] args)
        {
            // Using .Net Core Dependency Injection
            RegisterServices();

            // Setup Logger
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .CreateLogger();

            var tracrClient = _serviceProvider.GetService<TracrService>();

            //Get the shipment details for multiple shipment id
            List<InboundShipment> shipments = await tracrClient.GetAllInboundShipmentsAsync();

            

            DisposeServices();
        }

        private static void RegisterServices()
        {
            var services = new ServiceCollection();
            string configFile = Environment.GetEnvironmentVariable("TRACR_CLI_CONFIG_FILE") ?? $"appsettings.json";

            if (!File.Exists(configFile))
            {
                Log.Error($"Unable to open {configFile}.");
            }

            // Build configuration
            configuration = new ConfigurationBuilder()
                .AddJsonFile(configFile, false)
                .Build();

            // Add access to generic IConfigurationRoot
            services.AddSingleton<IConfigurationRoot>(configuration);

            services.AddLogging();

            services.AddTracrClient(new TracrClientOptions()
            {
                ApiUrl= configuration["TracrApi:Url"],
                TokenUrl = configuration["TracrApi:OAuthToken:TokenUrl"],
                ClientId = configuration["TracrApi:OAuthToken:ClientId"],
                ClientSecret = configuration["TracrApi:OAuthToken:ClientSecret"],
            }
            );

            _serviceProvider = services.BuildServiceProvider();
        }

        private static void DisposeServices()
        {
            switch (_serviceProvider)
            {
                case null:
                    return;
                case IDisposable disposable:
                    disposable.Dispose();
                    break;
            }
        }
    }
}
